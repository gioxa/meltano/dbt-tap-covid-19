# dbt | tap-covid-19

This [dbt](https://github.com/fishtown-analytics/dbt) package contains data models for [tap-covid-19](https://gitlab.com/gioxa/meltano/tap-covid-19).
