with source as (

    select * FROM {{ref('covid_19_jh_source')}}

),

renamed as (

select 
      sum(confirmed) as confirmed,
      sum(deaths) as deaths,
      country_region as country, 
      CAST (date as date) as idate
      
      from source 

      GROUP BY (date, country_region)
       
      ORDER BY date DESC

)

SELECT * from renamed
