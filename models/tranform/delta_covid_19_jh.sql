with source as (

    select * FROM {{ref('covid_19_jh')}}

),

renamed as (

    SELECT idate,country, confirmed, 

        CASE
        WHEN country LIKE lag(country) OVER (ORDER BY country, idate )
            THEN confirmed - lag(confirmed) OVER (ORDER BY country, idate )
        ELSE confirmed
        END
        as delta_confirmed,

	deaths,

        CASE
        WHEN country LIKE lag(country) OVER (ORDER BY country, idate )
            THEN deaths - lag(deaths) OVER (ORDER BY country, idate )
        ELSE deaths
        END
        as delta_deaths

    from source
)

 SELECT * from renamed
