WITH renamed AS (
    --fix china/Main land china issue
    --remove Null items so later we can recalculate active for the missing actives
         SELECT 
	         CASE
			  WHEN deaths is Null THEN 0
			  ELSE deaths
			 END as deaths
	        ,CASE
			  WHEN recovered is Null THEN 0
			  ELSE recovered
			 END as recovered
	        ,active
            ,CASE
			  WHEN confirmed is Null THEN 0
			  ELSE confirmed
			 END as confirmed
         --- fix china/mainlandchina 
	        ,CASE
			  WHEN country_region::text ~~ 'Mainland China' THEN 'China'
			  ELSE country_region
			END as fcountry
           ,date
           ,province_state
           FROM {{ref('covid_19_jh_source')}}
	       ORDER by date,fcountry 
        ),

rename_5 AS (
            SELECT deaths,recovered,active,confirmed
        --- fix HongKong
	        ,CASE
			  WHEN fcountry::text ~~ 'China' and province_state::text ~~ 'Hong Kong' THEN 'Hong Kong'
			  ELSE fcountry
			END as fcountry
            ,date
            from renamed
),	
renamed1 AS (
    --calculate the actives
    --calculate the delta days or the new ones for confirmed/deaths/recovered
	   SELECT date,fcountry,confirmed, 
        CASE
			  WHEN active is Null THEN ((confirmed - deaths) - recovered)
			  ELSE active
			END as factive 
        ,CASE
        WHEN fcountry LIKE lag(fcountry) OVER (ORDER BY fcountry, date )
            THEN confirmed - lag(confirmed) OVER (ORDER BY fcountry, date )
        ELSE confirmed
        END
        as new_confirmed
	    ,deaths
        ,CASE
            WHEN fcountry LIKE lag(fcountry) OVER (ORDER BY fcountry, date )
                THEN deaths - lag(deaths) OVER (ORDER BY fcountry, date )
            ELSE deaths
        END
        as new_deaths
	    ,recovered
	     ,CASE
            WHEN fcountry LIKE lag(fcountry) OVER (ORDER BY fcountry, date )
                THEN recovered - lag(recovered) OVER (ORDER BY fcountry, date )
            ELSE recovered
        END as new_recovered
    from rename_5
   ),
   
renamed2 as	( 
    --finaly calculate the delta actives
         SELECT 
				 date::date AS idate
				,fcountry as country
				,factive as active
				,CASE
                    WHEN fcountry LIKE lag(fcountry) OVER (ORDER BY fcountry, date )
                        THEN factive - lag(factive) OVER (ORDER BY fcountry, date )
                    ELSE factive
                 END as new_active
				,recovered ,new_recovered
				,deaths ,new_deaths
				,confirmed , new_confirmed 
		FROM renamed1
	),

renamed3 as ( 
    --generate the numbers per date and per country
         SELECT sum(confirmed) AS confirmed,
                sum(deaths) AS deaths,
                sum(recovered) as recovered,
                sum(active) as active,
                sum(new_confirmed) AS new_confirmed,
                sum(new_deaths) AS new_deaths,
                sum(new_recovered) as new_recovered,
                sum(new_active) as new_active,
                country,
                idate
          FROM renamed2
          GROUP BY renamed2.idate, renamed2.country
          ORDER BY renamed2.idate ASC
)
--put the data out
   SELECT   *  FROM renamed3
